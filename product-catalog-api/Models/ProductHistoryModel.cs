﻿using System;
using System.ComponentModel.DataAnnotations;

namespace product_catalog_api.Models
{
    public class ProductHistoryModel
    {
        [Key]
        public int id { get; set; }
        public int product_id { get; set; }
        public decimal price { get; set; }
        public DateTime time { get; set; }
    }
}
