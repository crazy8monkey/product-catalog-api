﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;

namespace product_catalog_api.Models
{
    public class ProductsDBContext : DbContext
    {

        IConfiguration _configuration;
        public ProductsDBContext(IConfiguration iconfiguration)
        {
            //grabbing app settings
            _configuration = iconfiguration;
        }

        public virtual DbSet<CatalogModel> Catalog { get; set; }
        public virtual DbSet<ProductModel> Product { get; set; }
        public virtual DbSet<ProductHistoryModel> ProductHistory { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(_configuration.GetValue<string>("ConnectionStrings:DBConnection"));
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("dbo");
            modelBuilder.Entity<CatalogModel>().ToTable("catalogs");
            modelBuilder.Entity<ProductModel>().ToTable("product");
            modelBuilder.Entity<ProductHistoryModel>().ToTable("product_history");
            base.OnModelCreating(modelBuilder);
        }
    }
}
