﻿using System;
using System.ComponentModel.DataAnnotations;

namespace product_catalog_api.Models
{
    public class ProductModel
    {
        [Key]
        public int id { get; set; }
        public string product_name { get; set; }
        public decimal price { get; set; }
        public int catalog_id { get; set; }
    }
}
