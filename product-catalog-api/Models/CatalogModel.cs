﻿using System;
using System.ComponentModel.DataAnnotations;

namespace product_catalog_api.Models
{
    public class CatalogModel
    {
        [Key]
        public int id { get; set; }
        public string catalog_name { get; set; }
    }
}
