﻿using System;
using System.Collections.Generic;
using System.Linq;
using product_catalog_api.Models;

namespace product_catalog_api.Classes
{
    public class ProductHistory
    {
        private ProductsDBContext _dbContext;

        public ProductHistory(ProductsDBContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<ProductHistoryModel> List(int product_id)
        {
            return _dbContext.ProductHistory.Where(history => history.product_id == product_id).OrderByDescending(history => history.time).ToList();
        }

        public ProductHistoryModel Add(ProductModel product)
        {
            ProductHistoryModel newProductHistory = new ProductHistoryModel();
            newProductHistory.product_id = product.id;
            newProductHistory.price = product.price;
            newProductHistory.time = DateTime.Now;
            _dbContext.ProductHistory.Add(newProductHistory);
            _dbContext.SaveChanges();
            return newProductHistory;
        }

        public ProductHistoryModel Get(int history_id, int product_id)
        {
            return _dbContext.ProductHistory.Where(history => history.id == history_id)
                                            .Where(history => history.product_id == product_id).FirstOrDefault();
        }

        public ProductHistoryModel Update(int history_id, int product_id, ProductHistoryModel udateHistory)
        {
            ProductHistoryModel history = _dbContext.ProductHistory.Where(history => history.id == history_id)
                                            .Where(history => history.product_id == product_id).FirstOrDefault();

            history.price = udateHistory.price;
            history.time = DateTime.Now;
            _dbContext.SaveChanges();
            return history;
        }

        public void Remove(int history_id, int product_id)
        {
            ProductHistoryModel historyRecord = _dbContext.ProductHistory.Where(history => history.id == history_id)
                                                    .Where(history => history.product_id == product_id).FirstOrDefault();
            _dbContext.ProductHistory.Remove(historyRecord);
            _dbContext.SaveChanges();
        }
    }
}
