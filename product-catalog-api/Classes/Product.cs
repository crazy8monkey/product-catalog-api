﻿using System;
using System.Collections.Generic;
using System.Linq;
using product_catalog_api.Models;

namespace product_catalog_api.Classes
{
    public class Product
    {
        private ProductsDBContext _dbContext;

        public Product(ProductsDBContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<ProductModel> List(int catalog_id)
        {
            return _dbContext.Product.Where(products => products.catalog_id == catalog_id).ToList();
        }

        public ProductModel Add(ProductModel newProduct)
        {
            ProductModel finalProduct = new ProductModel();
            finalProduct.product_name = newProduct.product_name;
            finalProduct.price = newProduct.price;
            finalProduct.catalog_id = newProduct.catalog_id;

            _dbContext.Product.Add(finalProduct);
            _dbContext.SaveChanges();
            return finalProduct;
        }

        public ProductModel Get(int product_id, int catalog_id)
        {
            return _dbContext.Product.Where(product => product.id == product_id)
                                     .Where(product => product.catalog_id == catalog_id).FirstOrDefault();
        }

        public ProductModel Update(int product_id, int catalog_id, ProductModel updatePrice)
        {
            ProductModel product = _dbContext.Product.Where(product => product.id == product_id)
                                     .Where(product => product.catalog_id == catalog_id).FirstOrDefault();

            product.product_name = updatePrice.product_name;
            product.price = updatePrice.price;
            _dbContext.SaveChanges();
            return product;
        }

        public void Remove(int product_id, int catalog_id)
        {
            ProductModel product = _dbContext.Product.Where(product => product.id == product_id)
                                     .Where(product => product.catalog_id == catalog_id).FirstOrDefault();
            _dbContext.Product.Remove(product);
            _dbContext.SaveChanges();
        }


    }
}
