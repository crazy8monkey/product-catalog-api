﻿using System;
using System.Collections.Generic;
using System.Linq;
using product_catalog_api.Models;

namespace product_catalog_api.Classes
{
    public class Catalog
    {

        private ProductsDBContext _dbContext;

        public Catalog(ProductsDBContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<CatalogModel> List()
        {
            return _dbContext.Catalog.ToList(); ;
        }

        public CatalogModel Add(CatalogModel catalog)
        {
            _dbContext.Catalog.Add(catalog);
            _dbContext.SaveChanges();
            return catalog;
        }

        public CatalogModel Get(int catalog_id)
        {
            return _dbContext.Catalog.Where(catalog => catalog.id == catalog_id).FirstOrDefault();
        }

        public CatalogModel Update(int catalog_id, CatalogModel updateCatalog)
        {
            CatalogModel catalog = _dbContext.Catalog.Where(catalog => catalog.id == catalog_id).FirstOrDefault();
            catalog.catalog_name = updateCatalog.catalog_name;
            _dbContext.SaveChanges();
            return catalog;
        }

        public void Remove(int catalog_id)
        {
            CatalogModel catalog = _dbContext.Catalog.Where(catalog => catalog.id == catalog_id).FirstOrDefault();
            _dbContext.Catalog.Remove(catalog);
            _dbContext.SaveChanges();
        }
    }
}
