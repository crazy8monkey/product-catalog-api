﻿using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using product_catalog_api.Classes;
using product_catalog_api.Models;

namespace product_catalog_api.Controllers
{
    [Produces("application/json")]
    public class ProductController : Controller
    {
        Product product;
        ProductHistory productHistory;

        public ProductController(IConfiguration iconfiguration)
        {
            ProductsDBContext productsDBConext = new ProductsDBContext(iconfiguration);
            product = new Product(productsDBConext);
            productHistory = new ProductHistory(productsDBConext);
        }

        [HttpGet]
        [Route("api/catalogs/{catalog_id}/products")]
        public JsonResult List(int catalog_id)
        {
            return Json(product.List(catalog_id));
        }

        [HttpPost]
        [Route("api/catalogs/{catalog_id}/products")]
        public JsonResult Add(int catalog_id, [FromBody] ProductModel newProduct)
        {
            newProduct.catalog_id = catalog_id;
            ProductModel newProductObj = product.Add(newProduct);
            productHistory.Add(newProductObj);
            return Json(newProductObj);
        }

        [HttpGet]
        [Route("api/catalogs/{catalog_id}/products/{product_id}")]
        public JsonResult Get(int catalog_id, int product_id)
        {
            return Json(product.Get(product_id, catalog_id));
        }

        [HttpPut]
        [Route("api/catalogs/{catalog_id}/products/{product_id}")]
        public JsonResult Set(int catalog_id, int product_id, [FromBody] ProductModel updateProduct)
        {
            updateProduct.id = product_id;
            productHistory.Add(updateProduct);
            return Json(product.Update(product_id, catalog_id, updateProduct));
        }

        [HttpDelete]
        [Route("api/catalogs/{catalog_id}/products/{product_id}")]
        public JsonResult Delete(int catalog_id, int product_id)
        {
            //removing product history
            foreach (ProductHistoryModel history in productHistory.List(product_id))
            {
                productHistory.Remove(history.id, product_id);
            }
            //removing product
            product.Remove(product_id, catalog_id);
            return Json(new { success = true });
        }
    }
}
