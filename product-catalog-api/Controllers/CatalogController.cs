﻿using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using product_catalog_api.Classes;
using product_catalog_api.Models;

namespace product_catalog_api.Controllers
{
    [Produces("application/json")]
    public class CatalogController : Controller
    {

        Catalog catalog;
        ProductHistory productHistory;
        Product product;

        public CatalogController(IConfiguration iconfiguration)
        {
            ProductsDBContext productsDBConext = new ProductsDBContext(iconfiguration);
            catalog = new Catalog(productsDBConext);
            product = new Product(productsDBConext);
            productHistory = new ProductHistory(productsDBConext);
        }

        [HttpGet]
        [Route("api/catalogs")]
        public JsonResult List()
        {
            return Json(catalog.List());
        }

        [HttpPost]
        [Route("api/catalogs")]
        public JsonResult Add([FromBody] CatalogModel newCatalog)
        {
            return Json(catalog.Add(newCatalog));
        }

        [HttpGet]
        [Route("api/catalogs/{catalog_id}")]
        public JsonResult Get(int catalog_id)
        {
            return Json(catalog.Get(catalog_id));
        }

        [HttpPut]
        [Route("api/catalogs/{catalog_id}")]
        public JsonResult Set(int catalog_id, [FromBody] CatalogModel updateCatalog)
        {
            return Json(catalog.Update(catalog_id, updateCatalog)); ;
        }


        [HttpDelete]
        [Route("api/catalogs/{catalog_id}")]
        public JsonResult Delete(int catalog_id)
        {
            //removing product history
            foreach (ProductModel single_product in product.List(catalog_id))
            {
                foreach (ProductHistoryModel history in productHistory.List(single_product.id))
                {
                    productHistory.Remove(history.id, single_product.id);
                }
                product.Remove(single_product.id, catalog_id);
            }

            catalog.Remove(catalog_id);
            return Json(new { success = true });
        }

    }
}
