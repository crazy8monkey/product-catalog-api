﻿using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using product_catalog_api.Classes;
using product_catalog_api.Models;

namespace product_catalog_api.Controllers
{
    [Produces("application/json")]
    public class HistoryController : Controller
    {

        ProductHistory history;

        public HistoryController(IConfiguration iconfiguration)
        {
            ProductsDBContext productsDBConext = new ProductsDBContext(iconfiguration);
            history = new ProductHistory(productsDBConext);
        }

        [HttpGet]
        [Route("api/products/{product_id}/history")]
        public JsonResult List(int product_id)
        {
            return Json(history.List(product_id));
        }

        [HttpPost]
        [Route("api/products/{product_id}/history")]
        public JsonResult Add(int product_id, [FromBody] ProductModel product)
        {
            product.id = product_id;
            return Json(history.Add(product));
        }

        [HttpGet]
        [Route("api/products/{product_id}/history/{history_id}")]
        public JsonResult Get(int product_id, int history_id)
        {
            return Json(history.Get(history_id, product_id));
        }

        [HttpPut]
        [Route("api/products/{product_id}/history/{history_id}")]
        public JsonResult Set(int product_id, int history_id, [FromBody] ProductHistoryModel updateHistory)
        {
            return Json(history.Update(history_id, product_id, updateHistory));
        }

        [HttpDelete]
        [Route("api/products/{product_id}/history/{history_id}")]
        public JsonResult Delete(int product_id, int history_id)
        {
            history.Remove(history_id, product_id);
            return Json(new { success = true });
        }
    }
}
