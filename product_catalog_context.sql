CREATE DATABASE products_context;

CREATE TABLE catalogs (
    id INT IDENTITY(1,1),
    catalog_name VARCHAR(255)
);

CREATE TABLE product (
    id INT IDENTITY(1,1),
    product_name VARCHAR(255),
    price DECIMAL(10,2),
    catalog_id INT
);

CREATE TABLE product_history (
    id INT IDENTITY(1,1),
    product_id INT,
    price DECIMAL(10,2),
    time DATETIME
);
